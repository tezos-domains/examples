# Tezos Domains Examples

This project requires [npm](https://www.npmjs.com/) or [Yarn](https://yarnpkg.com/). We assume npm for this README.

1. Start by installing packages:
    ```
    npm install
    ```
2. Create your own `.env` file by copying `default.env` to `.env`. You can fill in `SIGNING_KEY` and `ADDRESS` which are required examples which are required for some of the examples.
    ```
    cp default.env .env
    code .env
    ```

3. Individual examples can be run using `ts-node`:
    ```
    npx ts-node src/resolve-domain.ts
    ```

That's it! Have fun.
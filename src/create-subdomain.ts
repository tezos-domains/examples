import { InMemorySigner } from '@taquito/signer';
import { TezosToolkit } from '@taquito/taquito';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import { isTezosDomainsSupportedNetwork, RecordMetadata } from '@tezos-domains/core';
import { config } from 'dotenv';

config();

(async () => {
    if (!process.env.SIGNING_KEY) throw new Error('No SIGNING_KEY defined. Please set up your .env file');
    if (!process.env.RPC_URL) throw new Error('No RPC_URL defined. Please set up your .env file');
    if (!isTezosDomainsSupportedNetwork(process.env.NETWORK)) throw new Error(`Specified NETWORK ${process.env.NETWORK} is not supported.`);

    const tezos = new TezosToolkit(process.env.RPC_URL);
    tezos.setSignerProvider(await InMemorySigner.fromSecretKey(process.env.SIGNING_KEY));
    const client = new TaquitoTezosDomainsClient({ network: process.env.NETWORK, tezos });

    const label = `foobar-${Math.floor(Math.random() * 100000)}`;
    const parent = `bob.${client.validator.supportedTLDs[0]}`;

    // Create record
    const recordOperation = await client.manager.setChildRecord({ label, parent, owner: process.env.ADDRESS, address: process.env.ADDRESS, data: new RecordMetadata() });
    await recordOperation.confirmation();

    console.log(`Subdomain ${label}.${parent} has been created.`);

    // Create reverse record
    const reverseRecordOperation = await client.manager.claimReverseRecord({ owner: process.env.ADDRESS, name: `${label}.${parent}` });
    await reverseRecordOperation.confirmation();

    console.log(`Reverse record for ${process.env.ADDRESS} has been assigned to ${label}.${parent}.`);
})().catch(e => console.error(e));

import { InMemorySigner } from '@taquito/signer';
import { TezosToolkit } from '@taquito/taquito';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import { isTezosDomainsSupportedNetwork, RecordMetadata } from '@tezos-domains/core';
import { config } from 'dotenv';

config();

(async () => {
    if (!process.env.SIGNING_KEY) throw new Error('No SIGNING_KEY defined. Please set up your .env file');
    if (!process.env.RPC_URL) throw new Error('No RPC_URL defined. Please set up your .env file');
    if (!isTezosDomainsSupportedNetwork(process.env.NETWORK)) throw new Error(`Specified NETWORK ${process.env.NETWORK} is not supported.`);

    const tezos = new TezosToolkit(process.env.RPC_URL);
    tezos.setSignerProvider(await InMemorySigner.fromSecretKey(process.env.SIGNING_KEY));
    const client = new TaquitoTezosDomainsClient({ network: process.env.NETWORK, tezos });

    let name = `bob.${client.validator.supportedTLDs[0]}`;

    // set some openid claims (see https://openid.net/specs/openid-connect-core-1_0.html#StandardClaims for more)
    let data = new RecordMetadata();
    data.setJson('openid:name', 'Bob Vance');
    data.setJson('openid:email', 'bob@vance-refrigeration.com');
    data.setJson('openid:address', {
        street_address: '1725 Slough Avenue',
        locality: 'Scranton',
        region: 'Pennsylvania',
        country: 'United States'
    });
    const operation = await client.manager.updateRecord({ name, owner: process.env.ADDRESS, address: process.env.ADDRESS, data });
    await operation.confirmation();

    console.log(`Domain ${name} has been updated.`);
})().catch(e => console.error(e));

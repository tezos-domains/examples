import { InMemorySigner } from '@taquito/signer';
import { TezosToolkit } from '@taquito/taquito';
import { Tzip16Module } from '@taquito/tzip16';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import { generateNonce, isTezosDomainsSupportedNetwork, RecordMetadata } from '@tezos-domains/core';
import { config } from 'dotenv';

config();

(async () => {
    if (!process.env.SIGNING_KEY) throw new Error('No SIGNING_KEY defined. Please set up your .env file');
    if (!process.env.RPC_URL) throw new Error('No RPC_URL defined. Please set up your .env file');
    if (!isTezosDomainsSupportedNetwork(process.env.NETWORK)) throw new Error(`Specified NETWORK ${process.env.NETWORK} is not supported.`);

    const tezos = new TezosToolkit(process.env.RPC_URL);
    tezos.addExtension(new Tzip16Module());
    tezos.setSignerProvider(await InMemorySigner.fromSecretKey(process.env.SIGNING_KEY));
    const client = new TaquitoTezosDomainsClient({ network: process.env.NETWORK, tezos });

    const label = `bob`;
    const tld = client.validator.supportedTLDs[0];

    // Check if the name is not taken already
    const existing = await client.resolver.resolveNameToAddress(`${label}.${tld}`);
    if (existing) {
        throw new Error('Domain name taken.');
    }

    // generate commitment parameters with a random nonce
    const nonce = generateNonce();
    const params = {
        label,
        owner: process.env.ADDRESS,
        nonce
    };

    // Register for one year
    const duration = 365;

    // First step of registering a domain - create a commitment
    const commitOperation = await client.manager.commit(tld, params);
    await commitOperation.confirmation();

    // Show how much it costs
    const info = await client.manager.getAcquisitionInfo(`${label}.${tld}`);
    console.log(`Buying ${label}.${tld} for ${info.calculatePrice(duration)} mutez`);

    // Reveal on blockchain and confirm the registration for specified duration in days
    const buyOperation = await client.manager.buy(tld, { ...params, duration: 365, address: process.env.ADDRESS, data: new RecordMetadata(), nonce });
    await buyOperation.confirmation();

    console.log(`Domain ${label}.${tld} has been registered.`);
})().catch(e => console.error(e));

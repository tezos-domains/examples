import { TezosToolkit } from '@taquito/taquito';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import { Tzip16Module } from '@taquito/tzip16';
import { isTezosDomainsSupportedNetwork } from '@tezos-domains/core';
import { config } from 'dotenv';

config();

(async () => {
    if (!process.env.RPC_URL) throw new Error('No RPC_URL defined. Please set up your .env file');
    if (!isTezosDomainsSupportedNetwork(process.env.NETWORK)) throw new Error(`Specified NETWORK ${process.env.NETWORK} is not supported.`);
    
    const tezos = new TezosToolkit(process.env.RPC_URL);
    tezos.addExtension(new Tzip16Module());
    const client = new TaquitoTezosDomainsClient({ network: process.env.NETWORK, tezos });

    const name = `bob.${client.validator.supportedTLDs[0]}`;

    // resolve bob.tez and print the address
    const address = await client.resolver.resolveNameToAddress(name);
    console.log(address);

    // resolve the full record of bob.tez and print its information
    const record = await client.resolver.resolveDomainRecord(name);
    console.log(record);

    // print the associated name
    if (record) {
        console.log(record.data.getJson('openid:name'));
        console.log(record.data.getJson('openid:email'));
    }
})().catch(e => console.error(e));

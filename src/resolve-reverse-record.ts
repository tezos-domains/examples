import { TezosToolkit } from '@taquito/taquito';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import { Tzip16Module } from '@taquito/tzip16';
import { isTezosDomainsSupportedNetwork } from '@tezos-domains/core';
import { config } from 'dotenv';

config();

(async () => {
    if (!process.env.RPC_URL) throw new Error('No RPC_URL defined. Please set up your .env file');
    if (!isTezosDomainsSupportedNetwork(process.env.NETWORK)) throw new Error(`Specified NETWORK ${process.env.NETWORK} is not supported.`);
    
    const tezos = new TezosToolkit(process.env.RPC_URL);
    tezos.addExtension(new Tzip16Module());
    const client = new TaquitoTezosDomainsClient({ network: process.env.NETWORK, tezos });

    // resolve an address
    const address = await client.resolver.resolveAddressToName('tz1NjFgwjd7k1TJXLrzHjqS1TAFeiGCVVSLS');
    console.log(address);

    // print the full reverse record
    const record = await client.resolver.resolveReverseRecord('tz1NjFgwjd7k1TJXLrzHjqS1TAFeiGCVVSLS');
    console.log(record);
})().catch(e => console.error(e));
